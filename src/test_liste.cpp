#include "liste.hpp"

#include <iostream>
#include <cassert>

int main() {

  //Q1
//#define TEST_CONSTRUCTION_VIDE
#ifdef TEST_CONSTRUCTION_VIDE
  {
    //du bruit sur la pile
    int nimp[] = {1, 2, 3, 4, 5, 6, 7} ;
    //eviter un avertissement de variable non utilisee
    (void)nimp ;
  }
  {
    Liste l1 ;

    assert(l1.premiere == nullptr && "une liste correctement initialisee ne doit pas avoir de premiere cellule") ;
  }
#endif

  //Q2
//#define TEST_AJOUT_SUPPRESSION_EN_TETE
#ifdef TEST_AJOUT_SUPPRESSION_EN_TETE
  {
    Liste l1 ;
    l1.ajouter_en_tete(10) ;
    assert(l1.tete()->valeur == 10 && "la valeur en tete n'est pas la derniere ajoutee") ;
    l1.ajouter_en_tete(11) ;
    assert(l1.tete()->valeur == 11 && "la valeur en tete n'est pas la derniere ajoutee") ;
    l1.ajouter_en_tete(12) ;
    assert(l1.tete()->valeur == 12 && "la valeur en tete n'est pas la derniere ajoutee") ;
    l1.ajouter_en_tete(13) ;
    assert(l1.tete()->valeur == 13 && "la valeur en tete n'est pas la derniere ajoutee") ;

    l1.supprimer_en_tete() ;
    assert(l1.tete()->valeur == 12 && "la valeur en tete n'est pas la bonne apres suppression") ;
    l1.supprimer_en_tete() ;
    assert(l1.tete()->valeur == 11 && "la valeur en tete n'est pas la bonne apres suppression") ;
    l1.supprimer_en_tete() ;
    assert(l1.tete()->valeur == 10 && "la valeur en tete n'est pas la bonne apres suppression") ;
    l1.supprimer_en_tete() ;
    assert(l1.tete() == nullptr && "la liste ne devrait plus avoir de cellule") ;
  }
#endif

  //Q3
//#define TEST_TAILLE
#ifdef TEST_TAILLE
  {
    Liste l1 ;
    assert(l1.taille() == 0 && "la taille d'une liste vide devrait etre 0") ;
    l1.ajouter_en_tete(10) ;
    assert(l1.taille() == 1 && "la taille de la liste ne correspond pas au nombre d'element ajoutes") ;
    l1.ajouter_en_tete(11) ;
    assert(l1.taille() == 2 && "la taille de la liste ne correspond pas au nombre d'element ajoutes") ;
    l1.ajouter_en_tete(12) ;
    assert(l1.taille() == 3 && "la taille de la liste ne correspond pas au nombre d'element ajoutes") ;
    l1.ajouter_en_tete(13) ;
    assert(l1.taille() == 4 && "la taille de la liste ne correspond pas au nombre d'element ajoutes") ;

    l1.supprimer_en_tete() ;
    assert(l1.taille() == 3 && "la taille de la liste ne correspond pas au nombre d'element supprimes") ;
    l1.supprimer_en_tete() ;
    assert(l1.taille() == 2 && "la taille de la liste ne correspond pas au nombre d'element supprimes") ;
    l1.supprimer_en_tete() ;
    assert(l1.taille() == 1 && "la taille de la liste ne correspond pas au nombre d'element supprimes") ;
    l1.supprimer_en_tete() ;
    assert(l1.taille() == 0 && "la taille d'une liste vide devrait etre 0") ;
  }
#endif

  //Q4
//#define TEST_AFFICHAGE
#ifdef TEST_AFFICHAGE
  {
    Liste l1 ;
    l1.ajouter_en_tete(10) ;
    l1.ajouter_en_tete(11) ;
    l1.ajouter_en_tete(12) ;
    l1.ajouter_en_tete(13) ;

    std::cout << "attendu : [ 13 12 11 10 ]" << std::endl ;
    l1.afficher() ; // [ 13 12 11 10 ]
  }
#endif

  //Q6
//#define TEST_AJOUT_EN_QUEUE
#ifdef TEST_AJOUT_EN_QUEUE
  {
    Liste l1 ;
    l1.ajouter_en_queue(10) ;
    assert(l1.queue()->valeur == 10 && "la valeur en queue n'est pas la derniere ajoutee") ;
    l1.ajouter_en_queue(11) ;
    assert(l1.queue()->valeur == 11 && "la valeur en queue n'est pas la derniere ajoutee") ;
    l1.ajouter_en_queue(12) ;
    assert(l1.queue()->valeur == 12 && "la valeur en queue n'est pas la derniere ajoutee") ;
    l1.ajouter_en_queue(13) ;
    assert(l1.queue()->valeur == 13 && "la valeur en queue n'est pas la derniere ajoutee") ;
  }
#endif

  //Q7
//#define TEST_RECHERCHE
#ifdef TEST_RECHERCHE
  {
    Liste l1 ;
    l1.ajouter_en_tete(10) ;
    Cellule* cell10 = l1.tete() ;
    l1.ajouter_en_tete(11) ;
    Cellule* cell11 = l1.tete() ;
    l1.ajouter_en_tete(12) ;
    Cellule* cell12 = l1.tete() ;
    l1.ajouter_en_tete(13) ;
    Cellule* cell13 = l1.tete() ;

    assert(l1.rechercher(10) == cell10 && "erreur, la cellule contenant 10 n'a pas été trouvée ou n'a pas la bonne adresse.") ;
    assert(l1.rechercher(11) == cell11 && "erreur, la cellule contenant 11 n'a pas été trouvée ou n'a pas la bonne adresse.") ;
    assert(l1.rechercher(12) == cell12 && "erreur, la cellule contenant 12 n'a pas été trouvée ou n'a pas la bonne adresse.") ;
    assert(l1.rechercher(13) == cell13 && "erreur, la cellule contenant 13 n'a pas été trouvée ou n'a pas la bonne adresse.") ;

    assert(l1.rechercher(14) == nullptr) ;
  }
#endif

  //Q8
//#define TEST_COPIE
#ifdef TEST_COPIE
  {
    Liste l1 ;
    l1.ajouter_en_tete(10) ;
    l1.ajouter_en_tete(11) ;
    l1.ajouter_en_tete(12) ;
    l1.ajouter_en_tete(13) ;

    {
      Liste l2 = l1 ;

      std::cout << "attendu pour l2 : [ 13 12 11 10 ]" << std::endl ;
      l2.afficher() ;

      //commenter cette ligne pour eviter la premiere erreur
      l1.tete()->valeur = 20 ;

      std::cout << "attendu pour l2 : [ 13 12 11 10 ]" << std::endl ;
      l2.afficher() ;
      assert(l2.tete()->valeur == 13 && "erreur, la liste l2 n'a pas la bonne valeur en tete.") ;
      
      //commenter cette ligne pour eviter la seconde erreur
      l2.ajouter_en_queue(9) ;

      std::cout << "attendu pour l1 : [ 20 12 11 10 ]" << std::endl ;
      l1.afficher() ;
      assert(l1.queue()->valeur == 10 && "erreur, la liste l1 n'a pas la bonne valeur en queue.") ;
    }

    {
      Liste l2 ;
      l2.ajouter_en_tete(100) ;
      l2.ajouter_en_tete(110) ;
      l2.ajouter_en_tete(120) ;
      l2.ajouter_en_tete(130) ;

      l2 = l1 ;

      std::cout << "attendu pour l2 : [ 20 12 11 10 ]" << std::endl ;
      l2.afficher() ;

      //commenter cette ligne pour eviter la premiere erreur
      l1.tete()->valeur = 13 ;

      std::cout << "attendu pour l2 : [ 20 12 11 10 ]" << std::endl ;
      l2.afficher() ;
      assert(l2.tete()->valeur == 20 && "erreur, la liste l2 n'a pas la bonne valeur en tete.") ;
      
      //commenter cette ligne pour eviter la seconde erreur
      l2.ajouter_en_queue(9) ;

      std::cout << "attendu pour l1 : [ 13 12 11 10 ]" << std::endl ;
      l1.afficher() ;
      assert(l1.queue()->valeur == 10 && "erreur, la liste l1 n'a pas la bonne valeur en queue.") ;
    }

    std::cout << "attendu pour l1 : [ 13 12 11 10 ]" << std::endl ;
    l1.afficher() ;

  }
#endif

  return 0 ;
}
