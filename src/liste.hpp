#ifndef LIFAPC_LISTE_LISTE_HPP
#define LIFAPC_LISTE_LISTE_HPP

#include "cellule.hpp"

class Liste {

  public :

    /* Q1 : construction d'une liste vide */

    /* - definir les variables membres de Cellule et Liste
     * - initialiser correctement ces variables membres
     */
    Liste() ;

    /* Q2 : ajout, suppression et consultation en tete */

    /* simple acces a la variable membre */
    Cellule* tete() ;

    /* - fabriquer une nouvelle cellule contenant la valeur
     * - modifier le chainage de la liste pour la placer en tete
     */
    void ajouter_en_tete(int valeur) ;

    /* - recuperer l'adresse de la nouvelle tete
     * - supprimer la cellule actuellement en tete
     * - mettre a jour la tete avec la bonne adresse
     */
    void supprimer_en_tete() ;

    /* Q3 : taille de la liste */

    /* - ajouter une variable membre pour la stocker
     * - initialiser la taille dans le constructeur
     * - mettre a jour la taille lors des modifications de la liste
     * - la methode renvoie simplement la valeur stockee
     */
    int taille() ;

    /* Q4 : affichage de tous les elements */

    /* - parcourir toutes les cellules de la liste
     * - pour chaque cellule afficher sa valeur
     */
    void afficher() ;

    /* Q5 : destruction */

    /* s'assurer que toutes les cellules sont detruites
     * sous linux, valgrind est disponible
     */
    ~Liste() ;

    /* Q6 : ajout et consultation en queue */

    /* parcourir la liste pour trouver l'adresse de la derniere cellule */
    Cellule* queue() ;

    /* - creer une nouvelle cellule avec la valeur
     * - recuperer l'adresse de la derniere cellule
     * - chainer la nouvelle cellule apres la derniere cellule 
     */
    void ajouter_en_queue(int valeur) ;

    /* Q7 : recherche d'une valeur dans la liste */

    /* - parcourir toutes les cellule
     * - si la valeur de la cellule correspond renvoyer l'adresse de la cellule
     * - si aucune cellule ne correspond renvoyer nullptr 
     */
    Cellule* rechercher(int valeur) ;

    /* Q8 recopie de la liste */

    /* - pour chaque cellule de l'autre liste
     * - créer une nouvelle cellule avec la même valeur
     * - chaîner les nouvelles cellules dans le même ordre que la liste copiee
     */
    Liste(Liste& autre) ;

    /* - détruire toutes les cellules actuelles
     * - idem que pour la construction par copie
     */
    Liste& operator=(Liste& autre) ;

    /* variables membres */

    /* premiere : adresse de la premiere cellule de la liste */

} ;

#endif
