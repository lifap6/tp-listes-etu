#include "liste.hpp"

#include <iostream>
#include <cassert>

/* Avertissement :
 *
 * toutes les implémentations proposées ici de base sont stupides et ne visent
 * qu'à faire en sorte que le code compile. Sentez-vous libre de réécrire,
 * commenter ou supprimer chaque ligne de code, en particulier les return
 */

/* Q1 : construction d'une liste vide */

/* - definir les variables membres de Cellule et Liste
 * - initialiser correctement ces variables membres
 */

Liste::Liste() {
  /* votre code ici */
  std::cout << "construction d'une liste a l'adresse " << this << std::endl ;
}

/* Q2 : ajout, suppression et consultation en tete */

/* simple acces a la variable membre */

Cellule* Liste::tete() {
  /* votre code ici */
  return nullptr ;
}

/* - fabriquer une nouvelle cellule contenant la valeur
 * - modifier le chainage de la liste pour la placer en tete
 */
void Liste::ajouter_en_tete(int valeur) {
  /* votre code ici */
}

/* - recuperer l'adresse de la nouvelle tete
 * - supprimer la cellule actuellement en tete
 * - mettre a jour la tete avec la bonne adresse
 */
void Liste::supprimer_en_tete() {
  /* votre code ici */
}


/* Q3 : taille de la liste */

/* - ajouter une variable membre pour la stocker
 * - initialiser la taille dans le constructeur
 * - mettre a jour la taille lors des modifications de la liste
 * - la methode renvoie simplement la valeur stockee
 */
int Liste::taille() {
  /* votre code ici */
  return 0 ;
}

/* Q4 : affichage de tous les elements */

/* - parcourir toutes les cellules de la liste
 * - pour chaque cellule afficher sa valeur
 */
void Liste::afficher() {
  /* votre code ici */
}

/* Q5 : destruction */

/* s'assurer que toutes les cellules sont detruites
 * sous linux, valgrind est disponible
 */
Liste::~Liste() {
  /* votre code ici */
  std::cout << "destruction de la liste a l'adresse " << this << std::endl ;
}

/* Q6 : ajout et consultation en queue */

/* parcourir la liste pour trouver l'adresse de la derniere cellule */
Cellule* Liste::queue() {
  /* votre code ici */
  return nullptr ;
}

/* - creer une nouvelle cellule avec la valeur
 * - recuperer l'adresse de la derniere cellule
 * - chainer la nouvelle cellule apres la derniere cellule 
 */
void Liste::ajouter_en_queue(int valeur) {
  /* votre code ici */
}

/* Q7 : recherche d'une valeur dans la liste */

/* - parcourir toutes les cellule
 * - si la valeur de la cellule correspond renvoyer l'adresse de la cellule
 * - si aucune cellule ne correspond renvoyer nullptr 
 */
Cellule* Liste::rechercher(int valeur) {
  /* votre code ici */
  return nullptr ;
}

/* Q8 recopie de la liste */

/* - pour chaque cellule de l'autre liste
 * - créer une nouvelle cellule avec la même valeur
 * - chaîner les nouvelles cellules dans le même ordre que la liste copiee
 */
Liste::Liste(Liste& autre) {
  /* votre code ici */
}

/* - détruire toutes les cellules actuelles
 * - idem que pour la construction par copie
 */
Liste& Liste::operator=(Liste& autre) {
  /* votre code ici */
  return *this ;
}
